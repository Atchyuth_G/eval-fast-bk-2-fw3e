#Machine Code Refactoring

#MachineNames Dictionary
Created 'MachineNames' dictionary to identify the name based on type and assigned values to it in constructor. This is to reduce multiple if-else conditions to identify the name.

#GetBaseColor class
Created 'GetBaseColor' class to identify the base color based on type. This is to reduce the redundancy as same set of code is available in two places.
