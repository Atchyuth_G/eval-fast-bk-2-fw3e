using System.Collections.Generic;

namespace FortCodeExercises.Exercise1
{
    public class Machine
    {
        readonly Dictionary<int, string> MachineNames = new Dictionary<int, string>();
        public string machineName = "";
        public int type = 0;
        public Machine()
        {
            MachineNames.Add(0, "bulldozer");
            MachineNames.Add(1, "crane");
            MachineNames.Add(2, "tractor");
            MachineNames.Add(3, "truck");
            MachineNames.Add(4, "car");
        }
        public string Name
        {
            get
            {
                var machineName = "";
                if (this.machineName == null || this.machineName == "")
                {
                    machineName = MachineNames[type];
                }
                return machineName;
            }
        }

        public string Description
        {
            get
            {
                var hasMaxSpeed = true;
                if (this.type == 3 || this.type == 4) hasMaxSpeed = false;
                else if (this.type == 1 || this.type == 2) hasMaxSpeed = true;
                string description = " " + this.Color + " " + this.Name + " [" + this.GetMaxSpeed(this.type, hasMaxSpeed) + "].";
                return description;
            }
        }

        public string Color
        {
            get
            {
                return GetBaseColor(this.type);
            }
        }

        public string TrimColor
        {
            get
            {
                var baseColor = GetBaseColor(this.type);
                var trimColor = "";
                if (this.type == 1 && this.IsDark(baseColor)) trimColor = "black";
                else if (this.type == 1 && !this.IsDark(baseColor)) trimColor = "white";
                else if (this.type == 2 && this.IsDark(baseColor)) trimColor = "gold";
                else if (this.type == 3 && this.IsDark(baseColor)) trimColor = "silver";
                return trimColor;
            }
        }

        public bool IsDark(string color)
        {
            var isDark = false;
            if (color == "red" || color == "green" || color == "black" || color == "crimson") isDark = true;
            else if (color == "yellow" || color == "white" || color == "beige" || color == "babyblue") isDark = false;
            return isDark;
        }

        public int GetMaxSpeed(int machineType, bool noMax = false)
        {
            var max = 70;
            if (machineType == 1 && !noMax) max = 70;
            else if (!noMax && machineType == 2) max = 60;
            else if (machineType == 0 && noMax) max = 80;
            else if (machineType == 2 && noMax) max = 90;
            else if (machineType == 4 && noMax) max = 90;
            else if (machineType == 1 && noMax) max = 75;
            return max;
        }

        public string GetBaseColor(int type)
        {
            string baseColor = "white";
            if (type == 0) baseColor = "red";
            else if (type == 1) baseColor = "blue";
            else if (type == 2) baseColor = "green";
            else if (type == 3) baseColor = "yellow";
            else if (type == 4) baseColor = "brown";

            return baseColor;
        }
    }
}